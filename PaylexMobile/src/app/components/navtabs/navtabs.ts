import { Component } from '@angular/core';

/**
 * Generated class for the NavtabsComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'navtabs',
  templateUrl: 'navtabs.html'
})
export class NavtabsComponent {

  text: string;

  constructor() {
    console.log('Hello NavtabsComponent Component');
    this.text = 'Hello World';
  }

}
