import { SearchresultComponent } from './../searchresult/searchresult';
import { NavController } from '@ionic/angular';
import { Component, EventEmitter, Output } from '@angular/core';
import { CompanyService } from './../../services/company.service';

@Component({
  selector: 'searchbar',
  templateUrl: 'searchbar.html',
  providers: [CompanyService]
})
export class SearchbarComponent {

  filtertext: string;
  @Output() Search = new EventEmitter<string>();
  SetSearching() {
    //this.navCtrl.push(SearchresultComponent, {filtertext: this.filtertext});
     // this.onSearch.emit(this.filtertext);
  }
  constructor(public navCtrl: NavController) {
  }
}

