import { AnimationBuilder, AnimationService } from 'css-animator';
import { NavParams } from '@ionic/angular';
import { Company } from './../../models/company';
import { CompanyService } from './../../services/company.service';
import { Component, Input, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'searchresult',
  templateUrl: 'searchresult.html',
  providers: [CompanyService]
})
export class SearchresultComponent implements OnInit {
  private animator: AnimationBuilder;
  @ViewChild('searchResult') searchResultElement;
  nomatchmessage: string;
  searchResultVisible = false;
  companies: Company[] = [];
  selectedCompany: Company;
  matchingCompanies: Company[] = [];
  private _filtertext = '';

  @Input()
  set filtertext(filtertext: string) {
    this._filtertext = filtertext;
    this.SearchCompanies();
  }

  get filtertext(): string { return this._filtertext; }

  constructor(private companyService: CompanyService, animationService: AnimationService, public navParams: NavParams) {
    this.animator = animationService.builder();
    this.animator.defaults.duration = 100;
    this.animator.defaults.delay = 0;
  }

  getCompanies(): void {
    this.companyService.getCompanies().then(companies => this.companies = companies);
  }

  ngOnInit(): void {
    this.getCompanies();
  }

  onSelect(company: Company): void {
    this.selectedCompany = company;
  }

  // gotoDetail(): void {
  //   this.router.navigate(['/persondetail', this.selectedPerson.id]);
  // }

  SearchCompanies(): void {
    if (this.filtertext.length > 2) {
      this.matchingCompanies = this.companies.filter(company => company.branche.toLowerCase().includes(this.filtertext.toLowerCase()) ||
        company.land.toLowerCase().includes(this.filtertext.toLowerCase()) ||
        company.vestigingsplaats.toLowerCase().includes(this.filtertext.toLowerCase())
      );
      if (this.matchingCompanies.length === 0) {
        this.nomatchmessage = 'No companies match the search criteria';
      } else {
        this.nomatchmessage = '';
        if (!this.searchResultVisible) { this.animateElemOpen(0); }
      }
    } else if (this.searchResultVisible) {
      this.animateElemClose(0);
    }
  }

  animateElemOpen(tries: number) {
    if (tries < 4) {
      this.animator.setType('zoomInDown').show(this.searchResultElement.nativeElement)
        .then(() => {
          this.searchResultVisible = true;
        })
        .catch(() => {
          this.animateElemOpen(tries + 1);
        });
    }
  }

  animateElemClose(tries: number) {
    if (tries < 4) {
      this.animator.setType('zoomOut').show(this.searchResultElement.nativeElement)
        .then(() => {
          this.matchingCompanies = [];
          this.searchResultVisible = false;
        })
        .catch(() => {
          this.animateElemClose(tries + 1);
        });
    } else {
      this.matchingCompanies = [];
      this.searchResultVisible = false;
    }
  }
}

