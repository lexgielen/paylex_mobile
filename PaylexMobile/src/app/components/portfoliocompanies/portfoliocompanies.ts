import { Component } from '@angular/core';

/**
 * Generated class for the PortfoliocompaniesComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'portfoliocompanies',
  templateUrl: 'portfoliocompanies.html'
})
export class PortfoliocompaniesComponent {

  text: string;

  constructor() {
    console.log('Hello PortfoliocompaniesComponent Component');
    this.text = 'Hello World';
  }

}
