import { Component, Input } from '@angular/core';

/**
 * Generated class for the AnnualfinanceComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'annualfinance',
  templateUrl: 'annualfinance.html'
})
export class AnnualfinanceComponent {

  open = false;
  @Input() public yeardata;
  constructor() {
  }

 Toggle(){
    this.open = !this.open;
 }

 staticAmountHTML(amount) {
  const html = '€' + Number(amount).toLocaleString('NL', { minimumFractionDigits: 2 });
  return html.replace(',', '<span class="nicecomma">,</span>');
}

staticAmount(amount) {
  return Number(amount).toLocaleString('NL', { minimumFractionDigits: 2 });
}

staticPercentageHTML(percentage) {
  const html =  Number(percentage).toLocaleString('NL', { minimumFractionDigits: 2 }) + '%';
  return html.replace(',', '<span class="nicecomma">,</span>');
}
}
