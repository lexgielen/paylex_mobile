import { DataShareService } from './../../services/datashare.service';
import { CreditReportPage } from './../../pages/credit-report/credit-report';
import { Component, Input } from '@angular/core';
import { NavController } from '@ionic/angular';
import { CompanyService } from './../../services/company.service';
import { Router } from '@angular/router';

/**
 * Generated class for the PortfolioCompanyItemComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'portfolio-company-item',
  templateUrl: 'portfolio-company-item.html'
})
export class PortfolioCompanyItemComponent {

  @Input() public company;
  companyReport : any;
  open = false;
  constructor(private router: Router, public navCtrl: NavController, private dataShareService: DataShareService, private companyService: CompanyService) {
    companyService.getRealCompany().then(element => {
      this.companyReport = element;
    });
  }

  openCompany() {
    this.open=true;
  }
  closeCompany(){
    this.open=false;
  }
  GoToReport(){
    this.dataShareService.set('report', this.companyReport);
    this.router.navigate(['/tabs/portfolio/report', {attr: 'report'}]);
    // this.navCtrl.push(CreditReportPage, this.companyReport);
  }
}
