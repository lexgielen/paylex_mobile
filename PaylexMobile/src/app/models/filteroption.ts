
export class FilterOption {
    name: String;
    selected: Boolean;

    constructor(name: string) {
        this.name = name;
        this.selected = false;
    }

    select() {
        this.selected = true;
    }

    deselect() {
        this.selected = false;
    }
}
