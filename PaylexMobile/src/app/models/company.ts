
export class Company {
  id: number;
  name: string;
  branche: string;
  oprichting: string;
  straatnaam: string;
  land: string;
  landcode: string;
  vestigingsplaats: string;
  postcode: string;
  rechtsvorm: string;
  huisnummer: string;
  volledigheid: number;
  grootte: number;
  kvk: string;
}

