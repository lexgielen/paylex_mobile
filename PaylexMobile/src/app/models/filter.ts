import { FilterOption } from './filteroption';

export class Filter {
    name: string;
    options: FilterOption[] = [];

    constructor(name: string) {
        this.name = name;
    }

    addOption(optionname) {
        const matchingoption = this.options.find(o => o.name === optionname);
        if (matchingoption === undefined) {
            this.options.push(new FilterOption(optionname));
        }
    }

    addOptions(optionnames: string[]) {
        optionnames.forEach(name => {
            const matchingoption = this.options.find(o => o.name === name);
            if (matchingoption === undefined) {
                this.options.push(new FilterOption(name));
            }
        });
    }

    unselectOptions() {
        this.options.forEach(option => {
            option.deselect();
        });
    }

    selectOption(optionname) {
        this.options.find(o => o.name === optionname).select();
    }

    unselectOption(optionname) {
        this.options.find(o => o.name === optionname).deselect();
    }

    isActive() {
        return (this.amountActive() > 0);
    }

    amountActive(): number {
        const selectedoptions = this.options.filter(function(option) {
            return option.selected === true;
        });
        return selectedoptions.length;
    }
}
