import { Filter } from './filter';

export class FilterHolder {
    filters: Filter[] = [];

    add(filter: Filter) {
        this.filters.push(filter);
    }

    get(filtername) {
      return this.filters.find(i => i.name === filtername);
    }

    getFilterNames(): string[] {
      const filternames = [];
      this.filters.forEach(filter => {
        filternames.push(filter.name);
      });
      return filternames;
    }

}
