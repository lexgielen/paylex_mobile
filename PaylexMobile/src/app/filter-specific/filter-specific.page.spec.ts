import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FilterSpecificPage } from './filter-specific.page';

describe('FilterSpecificPage', () => {
  let component: FilterSpecificPage;
  let fixture: ComponentFixture<FilterSpecificPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FilterSpecificPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FilterSpecificPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
