import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { FilterSpecificPage } from './filter-specific.page';

const routes: Routes = [
  {
    path: '',
    component: FilterSpecificPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),

  ],
  declarations: [FilterSpecificPage]
})
export class FilterSpecificPageModule {}
