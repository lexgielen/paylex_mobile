import { Filter } from 'src/app/models/filter';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DataShareService } from '../services/datashare.service';
import { MenuController } from '@ionic/angular';
import { fileURLToPath } from 'url';
import { filterQueryId } from '@angular/core/src/view/util';
import { routerNgProbeToken } from '@angular/router/src/router_module';
import { FilterHolder } from '../models/filterholder';

@Component({
  selector: 'app-filter-specific',
  templateUrl: './filter-specific.page.html',
  styleUrls: ['./filter-specific.page.scss'],
})
export class FilterSpecificPage implements OnInit {

  currentPageClass = this;
  filter: Filter;
  alphascrollelements: any[];
  alphascrolloptions: {key: string, options: {value: string, selected:boolean}[]}[];
  constructor(private route: ActivatedRoute,
    private dataShareService: DataShareService, private menu: MenuController, private router: Router) { }

  ngOnInit() {
    // const filtername = this.route.snapshot.paramMap.get('filter');

    // if(!this.isNullOrEmptyOrUndefined(filtername)) {
    //   this.loadOptions(filtername);
    // } else {
    //   this.router.navigate(['/tabs/dashboard/search']);
    // }
  }

  isNullOrEmptyOrUndefined (value) {
    return !value;
}

ionViewWillEnter(){
  const filtername = this.route.snapshot.paramMap.get('filter');

  if(!this.isNullOrEmptyOrUndefined(filtername)) {
    this.loadOptions(filtername);
  } else {
    this.router.navigate(['/tabs/dashboard/search']);
  }
}

loadOptions(filtername){
    let filterholder  = new FilterHolder();
    filterholder = this.dataShareService.get('filters');
    this.filter = filterholder.get(filtername);
    this.prepareAlphaScroll(this.filter.options);
}

prepareAlphaScroll(options){
  const array = [];
  options.forEach(element => {
    const elementchar = element.name.charAt(0).normalize('NFD').replace(/[\u0300-\u036f]/g, '');
    let lastchar = '';
    try{
        lastchar = array[array.length - 1].name.charAt(0).normalize('NFD').replace(/[\u0300-\u036f]/g, '');
        console.log(`lastchar was: ${lastchar}, elementchar was: ${elementchar}`)
        if(elementchar !== lastchar){
          array.push({name: elementchar, type: 'divider'});
          array.push({name: element.name, type: 'element', selected: false});
        } else {
          array.push({name: element.name, type: 'element', selected: false});
        }
      }catch(e){
        array.push({name: elementchar, type: 'divider'});
        array.push({name: element.name, type: 'element', selected: false});
   }
    // const result = array.find(obj => {
    //   return obj.key.normalize('NFD').replace(/[\u0300-\u036f]/g, '')
    //   ===
    //   element.name.charAt(0).normalize('NFD').replace(/[\u0300-\u036f]/g, '');
    // });
    // if(result === undefined){
    //   array.push({key: element.name.charAt(0), options: [{value: element.name, selected:false}]});
    // }else{
    //   result.options.push({value: element.name, selected:false});
  });
  this.alphascrollelements = array;
}

cancelFilter() {
  this.router.navigate(['/tabs/dashboard/search']);
 }

 submitFilter() {
   this.dataShareService.set('filter_' + this.filter.name, this.filter);
   this.router.navigate(['/tabs/dashboard/search']);
 }

 isFiltered() {
   if (this.filter) {
     this.filter.options.forEach(element => {
       if (element.selected) {
         return true;
       }
     });
   }
   return false;
 }

 removeFilters() {
   this.filter.options.forEach(element => {
     element.selected = false;
   });
 }
}
