import { Country } from './../models/country';
import { COUNTRIES } from './../data/countries';
import { Injectable } from '@angular/core';


@Injectable({ providedIn: 'root' })
export class CountryService {
  getCountries(): Promise<Country[]> {
    return Promise.resolve(COUNTRIES);
  }

  getCountry(Code: String): Promise<Country> {
    return this.getCountries()
      .then(countries => countries.find(country => country.Code === Code));
  }
}
