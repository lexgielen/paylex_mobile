import { CallsService } from './calls.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

@Injectable({ providedIn: 'root' })
export class ApiService {

  constructor(public httpClient: HttpClient, private callsService: CallsService) {

  }

/**
 * Retreive data of the current administration
 */
  GetCompanies(page, searchterm, filters): Observable<any> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Accept': 'application/json'
  });
    return this.httpClient.get(this.callsService.SearchCompaniesCall(searchterm, page, filters), { headers });
  }
}
