import { COMPANIES } from './../data/mock-companies';
import { Company } from './../models/company';
import { Injectable } from '@angular/core';
import { REALCOMPANY } from '../data/realcompany';


@Injectable({ providedIn: 'root' })
export class CompanyService {
  selectedCompany: any;

  getCompanies(): Promise<Company[]> {
    return Promise.resolve(COMPANIES);
  }

  getCompany(id: number): Promise<Company> {
    return this.getCompanies()
               .then(companies => companies.find(company => company.id === id));
  }

  getRealCompany(): Promise<any> {
    this.selectedCompany = REALCOMPANY;
    return Promise.resolve(REALCOMPANY);
  }
}
