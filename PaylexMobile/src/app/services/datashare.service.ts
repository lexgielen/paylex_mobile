import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class DataShareService {

  private data = {};

  set(attr, data) {
    this.data[attr] = data;
  }

  get(attr) {
    return this.data[attr];
  }

}
