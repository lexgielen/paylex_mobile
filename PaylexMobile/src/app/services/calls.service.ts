import { Injectable } from '@angular/core';

@Injectable()
export class CallsService {

  private baseUrl = 'https://thomas.paylex.nl/rest/';
  private searchCompaniesUrl1 = 'search/entities?page=';
  private searchCompaniesUrl2 = '&indices[]=company_identification&term="';

  SearchCompaniesCall(searchterm, page, filters): string {
    const call = this.baseUrl + this.searchCompaniesUrl1 + page + this.searchCompaniesUrl2 + searchterm + '"&uuid=';
    return call;
  }

}
