export const REALCOMPANY: any = {
  'entity': {
      'id': 'nzkevy3e35',
      'registered_company_name': 'HARRODS LIMITED',
      'company_registration_number': '00030209',
      'country_code': {
          'id': 826,
          'country': 'GB',
          'name': 'United Kingdom',
          'country_credit_ratings': [
              {
                  'id': 194,
                  'rating': 93
              }
          ]
      },
      'company_registration_date': '1889-11-20T00:00:00+00:00',
      'legal_form': 'Private limited with Share Capital',
      'establishments': [
          {
              'id': 33,
              'address': {
                  'id': 114,
                  'street': 'Harrold street',
                  'house_number': '22',
                  'house_number_addition1': null,
                  'house_number_addition2': null,
                  'zipcode': 'London',
                  'city': 'Lodnon',
                  'telephone': null,
                  'full_address': null
              },
              'websites': [],
              'activity_codes': [
                  {
                      'code': '5212',
                      'label_dutch': null,
                      'label_french': null,
                      'label_german': null,
                      'label_english': 'Other retail non-specialised stores',
                      'activity_code_version': null,
                      'description': null
                  }
              ],
              'rsin': null,
              'company_registration_sub_number': '0000'
          }
      ],
      'credit_scores': [
          {
              'id': 2,
              'current_common_credit_rating': '93',
              'current_common_rating_description': 'Very Low Risk',
              'current_credit_limit': 76150000,
              'current_provider_credit_rating': null,
              'current_provider_rating_description': null,
              'previous_common_credit_rating': '87',
              'previous_common_rating_description': 'Very Low Risk',
              'previous_credit_limit': 94000000
          }
      ],
      'profit_and_loss_accounts': [
          {
              'id': 27,
              'financial_year': 2018,
              'number_of_weeks': 53,
              'currency': 'GBP',
              'consolidated_accounts': '1',
              'revenue': 862500000,
              'operating_costs': 644100000,
              'operating_profit': 218400000,
              'wages_and_salaries': 130700000,
              'pension_costs': 4300000,
              'depreciation': null,
              'amortisation': 6000000,
              'financial_income': null,
              'financial_expenses': 27500000,
              'extraordinary_income': null,
              'extraordinary_costs': null,
              'profit_before_tax': 215900000,
              'tax': -39200000,
              'profit_after_tax': 176700000,
              'dividents': null,
              'minority_interests': null,
              'other_appropriations': null,
              'retained_profit': 51700000
          },
          {
              'id': 28,
              'financial_year': 2017,
              'number_of_weeks': 52,
              'currency': 'GBP',
              'consolidated_accounts': '1',
              'revenue': 919700000,
              'operating_costs': 666400000,
              'operating_profit': 253300000,
              'wages_and_salaries': 142500000,
              'pension_costs': 4500000,
              'depreciation': null,
              'amortisation': 1600000,
              'financial_income': null,
              'financial_expenses': 38000000,
              'extraordinary_income': null,
              'extraordinary_costs': null,
              'profit_before_tax': 233200000,
              'tax': -40700000,
              'profit_after_tax': 192500000,
              'dividents': null,
              'minority_interests': null,
              'other_appropriations': null,
              'retained_profit': 82500000
          },
          {
              'id': 29,
              'financial_year': 2016,
              'number_of_weeks': 52,
              'currency': 'GBP',
              'consolidated_accounts': '1',
              'revenue': 788900000,
              'operating_costs': -610800000,
              'operating_profit': 178100000,
              'wages_and_salaries': 125100000,
              'pension_costs': 4300000,
              'depreciation': null,
              'amortisation': 1300000,
              'financial_income': null,
              'financial_expenses': 28100000,
              'extraordinary_income': null,
              'extraordinary_costs': null,
              'profit_before_tax': 168000000,
              'tax': -38300000,
              'profit_after_tax': 129700000,
              'dividents': null,
              'minority_interests': null,
              'other_appropriations': null,
              'retained_profit': 29600000
          }
      ],
      'balance_sheets': [
          {
              'id': 8,
              'financial_year': 2018,
              'currency': 'GBP',
              'total_tangible_assets': 1443500000,
              'total_intangible_assets': 17500000,
              'total_fixed_assets': 1696300000,
              'total_inventories': 98900000,
              'trade_receivables': 33200000,
              'miscellaneous_receivables': 713300000,
              'total_receivables': 746500000,
              'cash': 83700000,
              'other_current_assets': 5300000,
              'total_current_assets': 934400000,
              'total_assets': 2630700000,
              'trade_payables': 169100000,
              'miscellaneous_liabilities': 135800000,
              'total_current_liabilities': 417800000,
              'miscellaneous_liabilities_due_after_one_year': 2700000,
              'total_long_term_liabilities': 217300000,
              'total_liabilities': 635100000,
              'called_up_share_capital': 19200000,
              'revenue_reserves': 1055000000,
              'other_reserves': 921400000,
              'total_shareholders_equity': 1995600000,
              'number_of_employees': 3924,
              'working_capital': 516600000,
              'net_worth': 1978100000
          },
          {
              'id': 9,
              'financial_year': 2017,
              'currency': 'GBP',
              'total_tangible_assets': 1165900000,
              'total_intangible_assets': 20000000,
              'total_fixed_assets': 1185900000,
              'total_inventories': 102900000,
              'trade_receivables': 41100000,
              'miscellaneous_receivables': 1019800000,
              'total_receivables': 1060900000,
              'cash': 31400000,
              'other_current_assets': 10400000,
              'total_current_assets': 1205600000,
              'total_assets': 2391500000,
              'trade_payables': 162100000,
              'miscellaneous_liabilities': 122000000,
              'total_current_liabilities': 360800000,
              'miscellaneous_liabilities_due_after_one_year': 17400000,
              'total_long_term_liabilities': 272200000,
              'total_liabilities': 633000000,
              'called_up_share_capital': 19200000,
              'revenue_reserves': 1016300000,
              'other_reserves': 723000000,
              'total_shareholders_equity': 1758500000,
              'number_of_employees': 4197,
              'working_capital': 844800000,
              'net_worth': 1738500000
          },
          {
              'id': 10,
              'financial_year': 2016,
              'currency': 'GBP',
              'total_tangible_assets': 1274800000,
              'total_intangible_assets': 9800000,
              'total_fixed_assets': 1303900000,
              'total_inventories': 92300000,
              'trade_receivables': 28400000,
              'miscellaneous_receivables': 945400000,
              'total_receivables': 973800000,
              'cash': 72700000,
              'other_current_assets': 41800000,
              'total_current_assets': 1180600000,
              'total_assets': 2484500000,
              'trade_payables': 113800000,
              'miscellaneous_liabilities': 148300000,
              'total_current_liabilities': 373600000,
              'miscellaneous_liabilities_due_after_one_year': 3200000,
              'total_long_term_liabilities': 1077900000,
              'total_liabilities': 1451500000,
              'called_up_share_capital': 19200000,
              'revenue_reserves': 1013400000,
              'other_reserves': 400000,
              'total_shareholders_equity': 1033000000,
              'number_of_employees': 4138,
              'working_capital': 807000000,
              'net_worth': 1023200000
          }
      ],
      'ratios': [
          {
              'id': 13,
              'year': null,
              'pre_tax_profit_margin': 25.03,
              'return_on_capital_employed': 9.75,
              'return_on_total_assets_employed': 8.2,
              'return_on_net_assets_employed': 10.81,
              'sales_and_net_working_capital': 1.67,
              'stock_turnover_ratio': 11.46,
              'debtor_days': 14.28,
              'creditor_days': 72.73,
              'current_ratio': 2.24,
              'liquidity_ratio': 1.99,
              'current_debt_ratio': 0.2,
              'gearing': 10.89,
              'equity_in_percentage': 76.37,
              'total_debt_ratio': 0.31
          },
          {
              'id': 14,
              'year': null,
              'pre_tax_profit_margin': 25.36,
              'return_on_capital_employed': 11.48,
              'return_on_total_assets_employed': 9.75,
              'return_on_net_assets_employed': 13.26,
              'sales_and_net_working_capital': 1.09,
              'stock_turnover_ratio': 11.18,
              'debtor_days': 16.26,
              'creditor_days': 64.15,
              'current_ratio': 3.34,
              'liquidity_ratio': 3.05,
              'current_debt_ratio': 0.2,
              'gearing': 15.48,
              'equity_in_percentage': 74.15,
              'total_debt_ratio': 0.35
          },
          {
              'id': 15,
              'year': null,
              'pre_tax_profit_margin': 21.3,
              'return_on_capital_employed': 7.95,
              'return_on_total_assets_employed': 6.76,
              'return_on_net_assets_employed': 16.26,
              'sales_and_net_working_capital': 0.98,
              'stock_turnover_ratio': 11.69,
              'debtor_days': 13.1,
              'creditor_days': 52.5,
              'current_ratio': 3.16,
              'liquidity_ratio': 2.91,
              'current_debt_ratio': 0.36,
              'gearing': 104.35,
              'equity_in_percentage': 41.74,
              'total_debt_ratio': 1.4
          }
      ],
      'company_director_relations': [
          {
              'id': 45,
              'resignation_date': null,
              'position': 'Director',
              'director': {
                  'id': 53,
                  'name': 'Mr Rajiv Lochan Assanand',
                  'gender': 'Male'
              }
          },
          {
              'id': 46,
              'resignation_date': null,
              'position': 'Director',
              'director': {
                  'id': 54,
                  'name': 'Mr Jeffrey  Byrne',
                  'gender': 'Male'
              }
          },
          {
              'id': 47,
              'resignation_date': null,
              'position': 'Director',
              'director': {
                  'id': 55,
                  'name': 'Mr James Philip Healy',
                  'gender': 'Male'
              }
          },
          {
              'id': 48,
              'resignation_date': null,
              'position': 'Director',
              'director': {
                  'id': 56,
                  'name': 'Mr Michael Ashley Ward',
                  'gender': 'Male'
              }
          },
          {
              'id': 49,
              'resignation_date': null,
              'position': 'Director',
              'director': {
                  'id': 57,
                  'name': 'His Excellency Dr Hussain Ali A.a. Al-Abdulla',
                  'gender': null
              }
          },
          {
              'id': 50,
              'resignation_date': null,
              'position': 'Director',
              'director': {
                  'id': 58,
                  'name': 'His Excellency Sheik Hamad Bin Jassim Bin Jaber Al-Thani',
                  'gender': null
              }
          },
          {
              'id': 51,
              'resignation_date': null,
              'position': 'Director',
              'director': {
                  'id': 59,
                  'name': 'Mr Mansoor Ebrahim Si Al-Mahmoud',
                  'gender': 'Male'
              }
          },
          {
              'id': 52,
              'resignation_date': null,
              'position': 'Company Secretary',
              'director': {
                  'id': 60,
                  'name': 'Mr Daniel Jonathan Webster',
                  'gender': 'Male'
              }
          }
      ],
      'parent_companies': [
          {
              'id': 45,
              'percentage_owned': 100,
              'parent_company': {
                  'id': null,
                  'registered_company_name': 'HARRODS (UK) LIMITED',
                  'company_registration_number': '01889348',
                  'country_code': {
                      'id': 826,
                      'country': 'GB',
                      'name': 'United Kingdom',
                      'country_credit_ratings': [
                          {
                              'id': 194,
                              'rating': 93
                          }
                      ]
                  },
                  'company_registration_date': null,
                  'legal_form': null,
                  'establishments': [
                      {
                          'id': 78,
                          'address': {
                              'id': 87,
                              'street': 'Brompton',
                              'house_number': '87-135',
                              'house_number_addition1': null,
                              'house_number_addition2': null,
                              'zipcode': 'LONDON,',
                              'city': 'Road, Knightsbridge',
                              'telephone': null,
                              'full_address': null
                          },
                          'websites': [],
                          'activity_codes': [],
                          'rsin': null,
                          'company_registration_sub_number': '0000'
                      }
                  ],
                  'credit_scores': [],
                  'profit_and_loss_accounts': [],
                  'balance_sheets': [],
                  'ratios': [],
                  'company_director_relations': [],
                  'parent_companies': [
                      {
                          'id': 46,
                          'percentage_owned': 100,
                          'parent_company': {
                              'id': null,
                              'registered_company_name': 'QATAR HOLDING LLC',
                              'company_registration_number': null,
                              'country_code': {
                                  'id': 634,
                                  'country': 'QA',
                                  'name': 'Qatar',
                                  'country_credit_ratings': [
                                      {
                                          'id': 192,
                                          'rating': 85
                                      }
                                  ]
                              },
                              'company_registration_date': null,
                              'legal_form': null,
                              'establishments': [
                                  {
                                      'id': 79,
                                      'address': null,
                                      'websites': [],
                                      'activity_codes': [],
                                      'rsin': null,
                                      'company_registration_sub_number': '0000'
                                  }
                              ],
                              'credit_scores': [],
                              'profit_and_loss_accounts': [],
                              'balance_sheets': [],
                              'ratios': [],
                              'company_director_relations': [],
                              'parent_companies': [],
                              'pidi': {
                                  'id': 76,
                                  'paylex_international_data_identifier': '227276820'
                              },
                              'business_entity': null,
                              'description': null,
                              'incorporation_date': null,
                              'amount_of_employees': null,
                              'latest_turn_over_figure': null,
                              'date_of_latest_rating_change': null
                          },
                          'child_company': null
                      }
                  ],
                  'pidi': {
                      'id': 75,
                      'paylex_international_data_identifier': '227276820'
                  },
                  'business_entity': null,
                  'description': null,
                  'incorporation_date': null,
                  'amount_of_employees': null,
                  'latest_turn_over_figure': null,
                  'date_of_latest_rating_change': null
              },
              'child_company': null
          }
      ],
      'pidi': {
          'id': 30,
          'paylex_international_data_identifier': '665522111'
      },
      'business_entity': {},
      'description': 'Harrods',
      'incorporation_date': '1920-01-01T00:00:00+01:00',
      'amount_of_employees': 120,
      'latest_turn_over_figure': '862500000',
      'date_of_latest_rating_change': null
  },
  'additional_information': {
      'report_purchased': false,
      'amount_establishment': 1,
      'amount_member_tree_structure': 19,
      'amount_current_directors': 8,
      'amount_previous_directors': 62
  }
};
