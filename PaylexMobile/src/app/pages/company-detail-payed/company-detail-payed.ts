import { InAppPurchase } from '@ionic-native/in-app-purchase/ngx';

import { Company } from './../../models/company';
import { Component } from '@angular/core';
import { NavController, NavParams } from '@ionic/angular';

@Component({
  selector: 'page-company-detail-payed',
  templateUrl: 'company-detail-payed.html'
})
export class CompanyDetailPayedPage {
  company: Company;
  log: String = '';

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private iap: InAppPurchase
  ) {


  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CompanyDetailPayedPage');
    this.iap
    .getProducts(['paylexiriskabbo', 'p2'])
    .then(products => {
      this.addLog(products.toString());
      this.iap.restorePurchases().then(purchases => {
        purchases.forEach(element => {
          if (element.productId === 'p2') {
            this.addLog('consumable p2: ' + element.receipt);
            this.iap.consume(element.productType, element.receipt, element.signature)
            .then(data => {
                this.addLog('p2 consumed ' + data.receipt);
              })
            .catch(err => {
                this.addLog('error consuming p2 ' + err.message);
              });
          }
        });
        //  [{ productId: 'com.yourapp.prod1', 'title': '...', description: '...', price: '...' }, ...]
      })
      .catch(err => {
        this.addLog(err);
      });
      //  [{ productId: 'com.yourapp.prod1', 'title': '...', description: '...', price: '...' }, ...]
    })
    .catch(err => {
      this.addLog(err);
    });
  }

  ionViewWillEnter() {
    this.company = this.navParams.data.company;
  }

  addLog(logitem) {
    this.log += '\n' + logitem;
  }

  clearLog() {
    this.log = '';
  }

  buyCreditReport(company) {
    this.clearLog();
    // this.addLog('restoring purchases');

    // this.iap.restorePurchases()
    //   .then(purchases => {
    //     this.addLog('purchases restored');
    //     this.addLog(purchases);
    //   })
    //   .catch(err => {
    //     this.addLog('error restoring purchases');
    //     this.addLog(err);
    //   });

    this.addLog('buying p2');

    this.iap.buy('p2')
    .then(purchase => {
        this.addLog('p2 bought' + purchase.receipt);
        this.addLog('going to consume bought p2');
        this.iap.consume(purchase.productType, purchase.receipt, purchase.signature)
        .then(data => {
            this.addLog('p2 consumed: ' + data.receipt);
          })
        .catch(err => {
            this.addLog('error consuming p2 ' + err.message);
          });
        // {
        //   transactionId: ...
        //   receipt: ...
        //   signature: ...
        // }
      })
      .catch(err => {
        this.addLog('error buying p2 ' + err.message);
      });
  }

  buyPaylexIrisk() {
    this.clearLog();
    this.addLog('buying subscription');
    this.iap.buy('paylexiriskabbo')
      .then(data => {
        this.addLog('subscription bought succesfully ' + data.receipt);
      })
      .catch(err => {
        this.addLog('error buying subscription ' + err.message);
      });
  }
}
