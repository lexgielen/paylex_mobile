import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CompanyDetailPayedPage } from './company-detail-payed';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [
    CompanyDetailPayedPage,
  ],
  imports: [
    IonicModule,
    RouterModule.forChild([{path: '', component: CompanyDetailPayedPage}])
  ]
})
export class CompanyDetailPayedPageModule {}
