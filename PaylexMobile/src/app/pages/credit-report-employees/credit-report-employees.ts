import { PortfolioPage } from './../portfolio/portfolio';
import { Component, ViewChild, OnInit } from '@angular/core';
import { NavController, NavParams, MenuController } from '@ionic/angular';
import { Router, ActivatedRoute } from '@angular/router';
import { DataShareService } from 'src/app/services/datashare.service';

@Component({
  selector: 'page-credit-report-employees',
  templateUrl: 'credit-report-employees.html',
})
export class CreditReportEmployeesPage implements OnInit{

  report: any;
  navOptions = {
    animate: true,
    animation: 'ios-transition',
    direction: 'forward'
};
constructor(public navCtrl: NavController, private menu: MenuController,
  private router: Router, private dataShareService: DataShareService, private route: ActivatedRoute) {
}
ionViewWillEnter(){
  this.menu.close();
}
ngOnInit() {
  if(!this.isNullOrEmptyOrUndefined(this.dataShareService.get('report'))) {
    this.report = this.dataShareService.get('report');
  } else {
    this.router.navigate(['/tabs/portfolio']);
  }
}

  isNullOrEmptyOrUndefined (value) {
    return !value;
}

  openMenu() {
    this.menu.enable(true, 'creditreport');
    this.menu.open('creditreport');
  }
}
