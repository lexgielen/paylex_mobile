import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { CreditReportEmployeesPage } from './credit-report-employees';

@NgModule({
  declarations: [
    CreditReportEmployeesPage
  ],
  imports: [
    IonicModule,
    CommonModule,
    RouterModule.forChild([{path: '', component: CreditReportEmployeesPage}])
  ]
})
export class CreditReportEmployeesPageModule {}
