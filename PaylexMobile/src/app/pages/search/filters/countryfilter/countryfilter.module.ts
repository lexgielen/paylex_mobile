import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CountryfilterPage } from './countryfilter';

@NgModule({
  declarations: [
    CountryfilterPage,
  ],
  imports: [
    IonicModule,
    RouterModule.forChild([{path: '', component: CountryfilterPage}])
  ]
})
export class CountryfilterPageModule {}
