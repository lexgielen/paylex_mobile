import { CountryService } from '../../../../services/country.service';
import { Component, ViewChild } from '@angular/core';
import { Events, NavController, NavParams, Platform } from '@ionic/angular';

@Component({
  selector: 'page-countryfilter',
  templateUrl: 'countryfilter.html',
  providers: [CountryService]
})
export class CountryfilterPage {
  @ViewChild('searchbar') searchbar;
  countries: any[] = [];
  filtertext: string;
  platform: Platform;
  oldcountries: any[] = [];

  constructor(public events: Events,  platform: Platform, public navCtrl: NavController, public navParams: NavParams) {
    this.platform = platform;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Country filter');
  }

  ionViewWillEnter() {
    this.navParams.data.countries.forEach(element => {
      this.countries.push(Object.create(element));
    });
    this.oldcountries = this.navParams.data.countries;
  }

  cancelFilter() {
  //  this.navCtrl.pop();
  }

  selectedCountriesText() {
    let text = 'Selected countries';
    let first = true;
    this.countries.forEach(element => {
      if (element.selected && !first) {
        text += ', ' + element.country.Name;
      }
      if (element.selected && first) {
        text = element.country.Name;
        first = false;
      }
    });
    return text;
  }

  submitFilter() {
    this.events.publish('countryfilter:updated', this.countries);
    // this.navCtrl.pop();
  }

  isFiltered() {
    let result = false;
    if (this.countries) {
      this.countries.forEach(element => {
        if (element.selected) {
          result = true;
        }
      });
    }
    return result;
  }

  removeFilters() {
    this.countries.forEach(element => {
      element.selected = false;
    });
  }

  ionViewDidEnter() {
    setTimeout(() => {
      this.searchbar.setFocus();
    });
  }

  FlagCode(code) {
    return 'flag-icon flag-icon-' + code.toLowerCase();
  }

  Match(country): boolean {
    if (this.filtertext) {
      const match = country.country.Name.toLowerCase().includes(this.filtertext.toLowerCase());
      return match;
    }
    return false;
  }
}
