import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { BranchefilterPage } from './branchefilter';

@NgModule({
  declarations: [
    BranchefilterPage,
  ],
  imports: [
    IonicModule,
    RouterModule.forChild([{path: '', component: BranchefilterPage}])
  ]
})
export class BranchefilterPageModule {}
