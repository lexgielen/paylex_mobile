import { Component } from '@angular/core';
import { Events, NavController, NavParams } from '@ionic/angular';

@Component({
  selector: 'page-branchefilter',
  templateUrl: 'branchefilter.html',
})
export class BranchefilterPage {
  branches: any[] = [];
  oldbranches: any[] = [];
  constructor(public events: Events, public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BranchefilterPage');
  }

  ionViewWillEnter() {
    this.navParams.data.branches.forEach(element => {
      this.branches.push(Object.create(element));
    });
    this.oldbranches = this.navParams.data.branches;
  }

  cancelFilter() {
  //  this.navCtrl.pop();
  }

  submitFilter() {
    this.events.publish('branchefilter:updated', this.branches);
    // this.navCtrl.pop();
  }

  isFiltered() {
    let result = false;
    if (this.branches) {
      this.branches.forEach(element => {
        if (element.selected) {
          result = true;
        }
      });
    }
    return result;
  }

  removeFilters() {
    this.branches.forEach(element => {
      element.selected = false;
    });
  }
}
