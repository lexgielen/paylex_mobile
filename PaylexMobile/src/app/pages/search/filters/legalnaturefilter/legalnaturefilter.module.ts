import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { LegalnaturefilterPage } from './legalnaturefilter';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [
    LegalnaturefilterPage,
  ],
  imports: [
    IonicModule,
    RouterModule.forChild([{path: '', component: LegalnaturefilterPage}])
  ]
})
export class LegalnaturefilterPageModule {}
