import { Component } from '@angular/core';
import { Events, NavController, NavParams } from '@ionic/angular';

@Component({
  selector: 'page-legalnaturefilter',
  templateUrl: 'legalnaturefilter.html',
})
export class LegalnaturefilterPage {
  legalnatures: any[] = [];
  constructor(public events: Events, public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LegalnaturefilterPage');
  }

  ionViewWillEnter() {
    this.navParams.data.legalnatures.forEach(element => {
      this.legalnatures.push(Object.create(element));
    });
  }

  cancelFilter() {
  //  this.navCtrl.pop();
  }

  submitFilter() {
    this.events.publish('legalnaturefilter:updated', this.legalnatures);
    // this.navCtrl.pop();
  }

  isFiltered() {
    let result = false;
    if (this.legalnatures) {
      this.legalnatures.forEach(element => {
        if (element.selected) {
          result = true;
        }
      });
    }
    return result;
  }

  removeFilters() {
    this.legalnatures.forEach(element => {
      element.selected = false;
    });
  }
}
