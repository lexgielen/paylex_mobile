import { Component } from '@angular/core';
import { Events, NavController, NavParams } from '@ionic/angular';

@Component({
  selector: 'page-agefilter',
  templateUrl: 'agefilter.html',
})
export class AgefilterPage {
  ages: any[] = [];
  constructor(public events: Events, public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AgeFilterPage');
  }

  ionViewWillEnter() {
    this.navParams.data.ages.forEach(element => {
      this.ages.push(Object.create(element));
    });
  }

  cancelFilter() {
  //  this.navCtrl.pop();
  }

  submitFilter() {
    this.events.publish('agefilter:updated', this.ages);
    // this.navCtrl.pop();
  }

  isFiltered() {
    let result = false;
    if (this.ages) {
      this.ages.forEach(element => {
        if (element.selected) {
          result = true;
        }
      });
    }
    return result;
  }

  removeFilters() {
    this.ages.forEach(element => {
      element.selected = false;
    });
  }
}
