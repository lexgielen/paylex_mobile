import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { AgefilterPage } from './agefilter';

@NgModule({
  declarations: [
    AgefilterPage,
  ],
  imports: [
    IonicModule,
    RouterModule.forChild([{path: '', component: AgefilterPage}])
  ]
})
export class AgefilterPageModule {}
