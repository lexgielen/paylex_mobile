import { Component } from '@angular/core';
import { Events, NavController, NavParams } from '@ionic/angular';

@Component({
  selector: 'page-sizefilter',
  templateUrl: 'sizefilter.html',
})
export class SizefilterPage {
  sizes: any[] = [];
  constructor(public events: Events, public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SizeFilterPage');
  }

  ionViewWillEnter() {
    this.navParams.data.sizes.forEach(element => {
      this.sizes.push(Object.create(element));
    });
  }

  cancelFilter() {
  //  this.navCtrl.pop();
  }

  submitFilter() {
    this.events.publish('sizefilter:updated', this.sizes);
    // this.navCtrl.pop();
  }

  isFiltered() {
    let result = false;
    if (this.sizes) {
      this.sizes.forEach(element => {
        if (element.selected) {
          result = true;
        }
      });
    }
    return result;
  }

  removeFilters() {
    this.sizes.forEach(element => {
      element.selected = false;
    });
  }
}
