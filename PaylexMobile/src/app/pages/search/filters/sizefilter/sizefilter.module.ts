import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { SizefilterPage } from './sizefilter';

@NgModule({
  declarations: [
    SizefilterPage,
  ],
  imports: [
    IonicModule,
    RouterModule.forChild([{path: '', component: SizefilterPage}])
  ]
})
export class SizefilterPageModule {}
