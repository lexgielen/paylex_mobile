import { FilterHolder } from './../../models/filterholder';
import { DataShareService } from './../../services/datashare.service';
import { Events, Platform, IonSlides, MenuController } from '@ionic/angular';
import { ApiService } from './../../services/api.service';

import { LegalnaturefilterPage } from '../search/filters/legalnaturefilter/legalnaturefilter';
import { BranchefilterPage } from '../search/filters/branchefilter/branchefilter';
import { Country } from './../../models/country';
import { CompanyDetailPage } from './../company-detail/company-detail';
import { FilterPage } from '../search/filter/filter';
import { AgefilterPage } from '../search/filters/agefilter/agefilter';
import { Company } from './../../models/company';
import { CompanyService } from './../../services/company.service';
import { CountryService } from './../../services/country.service';
import { Component, OnInit, ViewChild } from '@angular/core';

import { CountryfilterPage } from '../search/filters/countryfilter/countryfilter';
import { SizefilterPage } from '../search/filters/sizefilter/sizefilter';
import { NavController, NavParams } from '@ionic/angular';
import { Filter } from 'src/app/models/filter';
import { ActivatedRoute, Router } from '@angular/router';
import { routerNgProbeToken } from '@angular/router/src/router_module';
@Component({
  selector: 'searchpage',
  templateUrl: 'search.html',
  providers: [CompanyService, CountryService],
  styleUrls: ['search.scss']
})
export class SearchPage implements OnInit {
  @ViewChild(IonSlides) slides: IonSlides;
  public showLeftButton: boolean;
  public showRightButton: boolean;
  @ViewChild('searchbar') searchbar;
  @ViewChild('filterbar') filterbar;
  switch: Boolean = false;
  filtertext: string;
  nomatchmessage: string;
  companies: Company[] = [];
  selectedCountry: Country;
  currentfilters: any = [
    { Legalnatures: null },
    { Branches: null },
    { Countries: null },
    { Sizes: null },
    { Ages: null }
  ];
  selectedFilter: 'Rechtsvorm';
  filtered: Boolean = false;
  singleCountry: Boolean = false;
  additionalfilter: Boolean;
  rechtsvormen: string[] = [];
  rechtsvormOpties: any[] = [];
  rechtsvormSelectie: string[] = [];
  ages: string[] = [];
  ageOptions: any[] = [];
  countries: any[] = [];
  countryOptions: any[] = [];
  sizes: string[] = [];
  sizeOptions: any[] = [];
  branches: string[] = [];
  brancheOptions: any[] = [];
  brancheSelectie: any[] = [];
  foundcompanies: any[] = [];
  scrollpage = 0;
  hasnextpage = false;
  totalfoundcompanies = 0;

  filters: any = [
    { name: 'Rechtsvorm', options: this.rechtsvormOpties, active: false, amount: 0},
    { name: 'Branche', options: this.brancheOptions, active: false, amount: 0 },
    { name: 'Land', options: this.countryOptions, active: false, amount: 0 },
    { name: 'Grootte', options: this.sizeOptions, active: false, amount: 0 },
    { name: 'Leeftijd', options: this.ageOptions, active: false, amount: 0 }
  ];

  filterHolder: FilterHolder = new FilterHolder();

  constructor(
    private route: ActivatedRoute,
    private menu: MenuController,
    private router: Router,
    private dataShareService: DataShareService,
    private countryService: CountryService,
    public events: Events,
    private platform: Platform,
    private companyService: CompanyService,
    public navCtrl: NavController,
    private apiService: ApiService
  ) {
    const elements = document.querySelectorAll('.tabbar');

    if (elements != null) {
      Object.keys(elements).map(key => {
        elements[key].style.display = 'none';
      });
    }

    events.subscribe('branchefilter:updated', branches => {
      this.updateFilterSelection(branches, 'Branche');
    });

    events.subscribe('sizefilter:updated', sizes => {
      this.updateFilterSelection(sizes, 'Grootte');
    });

    events.subscribe('legalnaturefilter:updated', legalnatures => {
      this.updateFilterSelection(legalnatures, 'Rechtsvorm');
    });

    events.subscribe('agefilter:updated', ages => {
      this.updateFilterSelection(ages, 'Leeftijd');
    });

    events.subscribe('countryfilter:updated', countries => {
      this.updateFilterSelection(countries, 'Land');
    });
  }

  updateFilterSelection(filteroptions, filtername) {
    let amount = 0;
    filteroptions.forEach(element => {
      if (element.selected) {
        amount++;
      }
    });

    const object = this.filters.filter(x => x.name === filtername)[0];
    object.options = filteroptions;
    object.active = amount > 0;
    object.amount = amount;

    if (filtername === 'Land') {
      if (amount === 1) {
        this.singleCountry = true;
        filteroptions.forEach(element => {
          if (element.selected) {
            this.selectedCountry = element.country;
          }
        });
      } else {
        this.singleCountry = false;
      }
    }

    this.updateFilterStatus(object);
  }

  updateFilterStatus(filter) {
    let filteractive = false;
    this.filters.sort(function(a, b) {
      return b.amount - a.amount;
    });
    this.filters.forEach(element => {
      if (element.active) {
        filteractive = true;
      }
    });
    this.filtered = filteractive;
  }

  removeFilters() {
    this.filters.forEach(filter => {
      filter.amount = 0;
      filter.active = false;
      filter.options.forEach(option => {
        option.selected = false;
      });
    });
    this.filtered = false;
  }

  async loadData(): Promise<boolean> {
    const promise = new Promise<boolean>((resolve, reject) => {
      this.setCompanies();
      this.setCountries();
      this.setAges();
      this.setSizes();
      resolve(true);
    });
    return promise;
  }
  setAges() {
    const filter = new Filter('age');
    const options = ['0-1', '1-2', '2-5', '5-10', '10-20', '20-50', '50-100'];
    filter.addOptions(options);
    this.filterHolder.add(filter);
  }

  setSizes() {
    const filter = new Filter('size');
    const options = [
      '1',
      '2',
      '3-5',
      '5-10',
      '10-20',
      '20-50',
      '50-100',
      '100-250',
      '250-500',
      '500+'
    ];
    filter.addOptions(options);
    this.filterHolder.add(filter);
  }

  setCompanies() {
    const branchefilter = new Filter('branche');
    const rechtsvormfilter = new Filter('legal form');
    this.companyService.getCompanies().then(companies => {
      this.companies = companies;
      this.companies.forEach(company => {
        branchefilter.addOption(company.branche);
        rechtsvormfilter.addOption(company.rechtsvorm);
      });
    });

    this.filterHolder.add(branchefilter);
    this.filterHolder.add(rechtsvormfilter);
  }

  setCountries() {
    const filter = new Filter('country');
    this.countryService.getCountries().then(countries => {
      this.countries = countries;
      this.countries.forEach(element => {
        filter.addOption(element.Name);
      });
    });
    this.filterHolder.add(filter);
  }

  ngOnInit() {
    const self = this;
    this.platform.ready().then(function() {
      self.loadData().then(function(succes) {
        self.dataShareService.set('filters', self.filterHolder);
        console.log('data loaded');
      }).catch(function(failed) {
        console.log('data not loaded');
      });
    });
  }

  ionViewWillEnter() {
    this.switch = !this.switch;

  }

  ionViewDidEnter() {
    const self = this;

    this.platform.ready().then(function() {
      self.searchbar.setFocus();
    });
  }

  goToFullFilters() {
    // this.navCtrl.push(FilterPage, { filters: this.filters });
  }

  goToCompanyDetail(selectedcompany) {
    // this.navCtrl.push(CompanyDetailPage, { company: selectedcompany });
  }

  onInput(x) {
    this.scrollpage = 0;
    this.apiService.GetCompanies(this.scrollpage, this.filtertext, null).subscribe(companies => {
      this.totalfoundcompanies = companies.pagination.total;
      this.foundcompanies = companies.entities;
      this.hasnextpage = companies.pagination.has_next_page;
      if (this.hasnextpage === true) {
        this.apiService.GetCompanies(this.scrollpage + 1, this.filtertext, null).subscribe(data => {
          this.scrollpage++;
          this.hasnextpage = data.pagination.has_next_page;
          data.entities.forEach(element => {
            this.foundcompanies.push(element);
          });
      });
      }
  }, error => {
    alert(`error in call: ${error.message}, status: ${error.status}`);
    });
  }

  LoadMoreCompanies(infiniteScroll) {
    if (this.hasnextpage === true) {
      this.scrollpage++;
      this.apiService.GetCompanies(this.scrollpage, this.filtertext, null).subscribe(data => {
        data.entities.forEach(element => {
          this.foundcompanies.push(element);
        });
        infiniteScroll.complete();
      });
    } else {
      infiniteScroll.complete();
    }
  }

  selectedCountryIcon(): string {
    return 'flag-icon flag-icon-' + this.selectedCountry.Code.toLowerCase();
  }
  getCompanyCountryIcon(company): string {
    return 'flag-icon flag-icon-' + company.entity.country_code.country.toLowerCase();
  }

  Match(company: Company) {
    if (!this.filtered) {
      if (this.filtertext && this.filtertext.length > 2) {
        if (
          this.selectedCountry &&
          !(company.landcode === this.selectedCountry.Code)
        ) {
          return false;
        }
        return (company.name + company.kvk)
          .toLowerCase()
          .includes(this.filtertext.toLowerCase());
      }
    } else {
      if (
        !this.filteredMatch(company) ||
        (this.selectedCountry &&
          !(company.landcode === this.selectedCountry.Code))
      ) {
        return false;
      }
      return true;
    }
    return false;
  }

  getFilterOptions(type: any) {
    return this.filters[type];
  }

  goToFilter(filter: Filter) {
    this.router.navigate(['/tabs/dashboard/search/filter', {filter: filter.name}]);
    // switch (filter.name.toLowerCase()) {
    //   case 'land':
    //     const countryfilter = this.filters.filter(x => x.name === 'Land')[0];
    //     // this.navCtrl.push(CountryfilterPage, {
    //     //   countries: countryfilter.options
    //     // });
    //     break;
    //   case 'grootte':
    //     const sizefilter = this.filters.filter(x => x.name === 'Grootte')[0];
    //     // this.navCtrl.push(SizefilterPage, {
    //     //   sizes: sizefilter.options
    //     // });
    //     break;
    //   case 'leeftijd':
    //     const agefilter = this.filters.filter(x => x.name === 'Leeftijd')[0];
    //     // this.navCtrl.push(AgefilterPage, {
    //     //   ages: agefilter.options
    //     // });
    //     break;
    //   case 'branche':
    //     const branchefilter = this.filters.filter(x => x.name === 'Branche')[0];
    //     // this.navCtrl.push(BranchefilterPage, {
    //     //   branches: branchefilter.options
    //     // });
    //     break;
    //   case 'rechtsvorm':
    //     const legalnaturefilter = this.filters.filter(
    //       x => x.name === 'Rechtsvorm'
    //     )[0];
    //     // this.navCtrl.push(LegalnaturefilterPage, {
    //     //   legalnatures: legalnaturefilter.options
    //     // });
    //     break;
    //   default:
    //     break;
    // }
  }

  filteredMatch(company): Boolean {
    if (
      this.brancheSelectie.indexOf(company.branche) !== -1 &&
      this.rechtsvormSelectie.indexOf(company.rechtsvorm) !== -1
    ) {
      return true;
    }
    return false;
  }
}
