import { FilterSpecificPageModule } from './../../filter-specific/filter-specific.module';
import { FilterSpecificPage } from './../../filter-specific/filter-specific.page';
import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgCircleProgressModule } from 'ng-circle-progress';
import { SearchPageRoutingModule } from './search.router.module';
import { SearchPage } from './search';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    NgCircleProgressModule.forRoot({
      // set defaults here
      radius: 100,
      outerStrokeWidth: 16,
      innerStrokeWidth: 8,
      outerStrokeColor: '#78C000',
      innerStrokeColor: '#C7E596',
      animationDuration: 300
    }),
    SearchPageRoutingModule,
    FilterSpecificPageModule
  ],
  declarations: [SearchPage]
})
export class SearchPageModule {}
