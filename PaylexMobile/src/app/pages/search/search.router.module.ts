import { FilterSpecificPage } from './../../filter-specific/filter-specific.page';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SearchPage } from './search';

const routes: Routes = [
  { path: '',
    component: SearchPage
},
{ path: 'filter',
component: FilterSpecificPage
}
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class SearchPageRoutingModule {}
