import { AgefilterPage } from './../filters/agefilter/agefilter';
import { SizefilterPage } from './../filters/sizefilter/sizefilter';
import { BranchefilterPage } from './../filters/branchefilter/branchefilter';
import { CountryfilterPage } from './../filters/countryfilter/countryfilter';
import { LegalnaturefilterPage } from './../filters/legalnaturefilter/legalnaturefilter';
import { Component } from '@angular/core';
import { Events, NavController, NavParams } from '@ionic/angular';

@Component({
  selector: 'page-filter',
  templateUrl: 'filter.html',
})
export class FilterPage {
  filters: any[] = [];
  constructor(public events: Events, public navCtrl: NavController, public navParams: NavParams) {
    events.subscribe('branchefilter:updated', branches => {
      this.updateFilter(branches, 'Branche');
    });

    events.subscribe('sizefilter:updated', sizes => {
      this.updateFilter(sizes, 'Grootte');
    });

    events.subscribe('legalnaturefilter:updated', legalnatures => {
      this.updateFilter(legalnatures, 'Rechtsvorm');
    });

    events.subscribe('agefilter:updated', ages => {
      this.updateFilter(ages, 'Leeftijd');
    });

    events.subscribe('countryfilter:updated', countries => {
      this.updateFilter(countries, 'Land');
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FilterPage');
  }

  updateFilter(options, filter) {
    const object = this.filters.filter(x => x.name === filter)[0];
    object.options = options;
    this.updateFilterSelection(object);
  }

  updateFilterSelection(filter) {
    let selection = '';
    filter.options.forEach(element => {
      if (element.selected) {
        if (selection === '') {
          if (filter.name === 'Land') {
            selection += element.country.Name;
          } else {
            selection += element.name;
          }
        } else {
          if (filter.name === 'Land') {
            selection += ', ' + element.country.Name;
          } else {
            selection += ', ' + element.name;
          }
        }
      }
    });
    filter.selection = selection;
  }

  ionViewWillEnter() {
    if (this.filters.length === 0) {
    this.navParams.data.filters.forEach(filterelement => {
      const filter = Object.create(filterelement);
      this.updateFilterSelection(filter);
      this.filters.push(filter);
    });
  }
  }

  goToFilter(filtername) {
    switch (filtername) {
      case 'Land':
        const countryfilter = this.filters.filter(x => x.name === 'Land')[0];
        // this.navCtrl.push(CountryfilterPage, {
        //   countries: countryfilter.options
        // });
        break;
      case 'Grootte':
        const sizefilter = this.filters.filter(x => x.name === 'Grootte')[0];
        // this.navCtrl.push(SizefilterPage, {
        //   sizes: sizefilter.options
        // });
        break;
      case 'Leeftijd':
        const agefilter = this.filters.filter(x => x.name === 'Leeftijd')[0];
        // this.navCtrl.push(AgefilterPage, {
        //   ages: agefilter.options
        // });
        break;
      case 'Branche':
        const branchefilter = this.filters.filter(x => x.name === 'Branche')[0];
        // this.navCtrl.push(BranchefilterPage, {
        //   branches: branchefilter.options
        // });
        break;
      case 'Rechtsvorm':
        const legalnaturefilter = this.filters.filter(
          x => x.name === 'Rechtsvorm'
        )[0];
        // this.navCtrl.push(LegalnaturefilterPage, {
        //   legalnatures: legalnaturefilter.options
        // });
        break;
      default:
        break;
    }
  }

  back() {
    // this.navCtrl.pop();
  }

}
