import { NgModule } from '@angular/core';
import { FilterPage } from './filter';
import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [
    FilterPage,
  ],
    imports: [
      IonicModule,
      RouterModule.forChild([{path: '', component: FilterPage}])
    ]
})
export class FilterPageModule {}
