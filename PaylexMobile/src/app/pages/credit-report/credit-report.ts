import { DataShareService } from './../../services/datashare.service';
import { PortfolioPage } from './../portfolio/portfolio';
import { Component, ViewChild, OnInit } from '@angular/core';
import { NavController, NavParams, MenuController } from '@ionic/angular';
import { Route, ActivatedRoute, Router } from '@angular/router';


@Component({
  selector: 'page-credit-report',
  templateUrl: 'credit-report.html',
})
export class CreditReportPage implements OnInit{

  report: any;
  navOptions = {
    animate: true,
    animation: 'ios-transition',
    direction: 'forward'
};

  constructor(public navCtrl: NavController,private route: ActivatedRoute,
    private dataShareService: DataShareService, private menu: MenuController, private router: Router) {
  }

  ionViewWillEnter(){
    this.menu.close();
  }

  ngOnInit() {
    if(!this.isNullOrEmptyOrUndefined(this.dataShareService.get('report'))) {
      this.report = this.dataShareService.get('report');
    } else {
      this.router.navigate(['/tabs/portfolio']);
    }
  }

  addressString(addressdata) {
    if (!this.isNullOrEmptyOrUndefined(addressdata.full_address)) {
      alert('address defined: ' + addressdata.full_address);
      return addressdata.full_address;
    }
    console.log(addressdata);
    let address = '';
    address = `${addressdata.street} ${addressdata.house_number} `;
    if (!this.isNullOrEmptyOrUndefined(addressdata.house_number_addition1)) {
      address = address + `${addressdata.house_number_addition1} `;
    }
    if (!this.isNullOrEmptyOrUndefined(addressdata.house_number_addition2)) {
      address = address + `${addressdata.house_number_addition2} `;
    }
    address = address + '\n';
    address = address + `${addressdata.zipcode}, ${addressdata.city}`;
    return address;
  }

  openMenu() {
    this.menu.enable(true, 'creditreport');
    this.menu.open('creditreport');
  }

  DateString(date) {
    return new Date(date).toLocaleDateString();
  }

  getYear(date) {
    return new Date(date).getFullYear();
  }

  isNullOrEmptyOrUndefined (value) {
    return !value;
}
}
