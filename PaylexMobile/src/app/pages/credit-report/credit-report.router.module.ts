import { CreditReportProfitAndCostsPage } from './../credit-report-profit-and-costs/credit-report-profit-and-costs';
import { CreditReportFinancialInfoPage } from './../credit-report-financial-info/credit-report-financial-info';
import { CreditReportEmployeesPage } from './../credit-report-employees/credit-report-employees';
import { CreditReportCompanyInfoPage } from './../credit-report-company-info/credit-report-company-info';
import { CreditReportAnnualOverviewPage } from './../credit-report-annual-overview/credit-report-annual-overview';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreditReportPage } from './credit-report';

const routes: Routes = [
//   {
//     path: 'summary',
//     component: CreditReportPage
// },
//   { path: 'annualoverview',
//     component: CreditReportAnnualOverviewPage
//   },
//   { path: 'companyinfo',
//   component: CreditReportCompanyInfoPage
// },
// { path: 'employees',
// component: CreditReportEmployeesPage
// },
// { path: 'financialinfo',
// component: CreditReportFinancialInfoPage
// },
// { path: 'profitandcosts',
// component: CreditReportProfitAndCostsPage
// },
{path: '',
redirectTo: '/summary',
pathMatch: 'prefix'
}
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class CreditReportPageRoutingModule {}
