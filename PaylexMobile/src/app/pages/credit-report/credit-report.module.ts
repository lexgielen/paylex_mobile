import { CommonModule } from '@angular/common';
import { CreditReportPageRoutingModule } from './credit-report.router.module';
import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { CreditReportPage } from './credit-report';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [
    CreditReportPage,
  ],
  imports: [
    IonicModule,
    CommonModule,
    RouterModule.forChild([{path: '', component: CreditReportPage}])  ]
})
export class CreditReportPageModule {}
