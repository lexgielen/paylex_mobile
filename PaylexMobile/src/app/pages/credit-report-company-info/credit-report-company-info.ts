import { Router, ActivatedRoute } from '@angular/router';
import { PortfolioPage } from './../portfolio/portfolio';
import { Component, ViewChild, OnInit } from '@angular/core';
import { NavController, NavParams, MenuController } from '@ionic/angular';
import { DataShareService } from 'src/app/services/datashare.service';

@Component({
  selector: 'page-credit-report-company-info',
  templateUrl: 'credit-report-company-info.html',
})
export class CreditReportCompanyInfoPage implements OnInit{

  report: any;
  navOptions = {
    animate: true,
    animation: 'ios-transition',
    direction: 'forward'
};
constructor(public navCtrl: NavController, private menu: MenuController,
  private router: Router, private dataShareService: DataShareService, private route: ActivatedRoute) {
}
ionViewWillEnter(){
  this.menu.close();
}
ngOnInit() {
    if(!this.isNullOrEmptyOrUndefined(this.dataShareService.get('report'))) {
    this.report = this.dataShareService.get('report');
  } else {
    this.router.navigate(['/tabs/portfolio']);
  }
}
  addressString(addressdata) {
    if (!this.isNullOrEmptyOrUndefined(addressdata.full_address)) {
      alert('address defined: ' + addressdata.full_address);
      return addressdata.full_address;
    }
    console.log(addressdata);
    let address = '';
    address = `${addressdata.street} ${addressdata.house_number} `;
    if (!this.isNullOrEmptyOrUndefined(addressdata.house_number_addition1)) {
      address = address + `${addressdata.house_number_addition1} `;
    }
    if (!this.isNullOrEmptyOrUndefined(addressdata.house_number_addition2)) {
      address = address + `${addressdata.house_number_addition2} `;
    }
    address = address + '\n';
    address = address + `${addressdata.zipcode}, ${addressdata.city}`;
    return address;
  }

  DateString(date) {
    return new Date(date).toLocaleDateString();
  }

  isNullOrEmptyOrUndefined (value) {
    return !value;
}
openMenu() {
  this.menu.enable(true, 'creditreport');
  this.menu.open('creditreport');
}
}
