import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CreditReportCompanyInfoPage } from './credit-report-company-info';

@NgModule({
  declarations: [
    CreditReportCompanyInfoPage
  ],
  imports: [
      IonicModule,
      CommonModule,
      RouterModule.forChild([{path: '', component: CreditReportCompanyInfoPage}])
  ],
})
export class CreditReportCompanyInfoPageModule {}
