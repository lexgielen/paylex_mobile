import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html',
})
export class SettingsPage implements OnInit {

  company: String = '';
  searchResult: String = '';
  ngOnInit(): void {
    // this.getCompanies();
  }
  SearchCompany(company): void {
    (company === 'Atlantis') ? this.searchResult = 'Bedrijf Gevonden!' : this.searchResult = 'Dit bedrijf bestaat niet';
  }
}

