import { PortfolioPage } from './portfolio';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: 'report',
  children: [
    {
      path: 'summary',
      loadChildren: './../credit-report/credit-report.module#CreditReportPageModule'
  },
  {
    path: 'annualoverview',
    loadChildren: './../credit-report-annual-overview/credit-report-annual-overview.module#CreditReportAnnualOverviewPageModule'
},
{
  path: 'companyinfo',
  loadChildren: './../credit-report-company-info/credit-report-company-info.module#CreditReportCompanyInfoPageModule'
},
{
path: 'employees',
loadChildren: './../credit-report-employees/credit-report-employees.module#CreditReportEmployeesPageModule'
},
{
  path: 'financialinfo',
  loadChildren: './../credit-report-financial-info/credit-report-financial-info.module#CreditReportFinancialInfoPageModule'
},
{
path: 'profitandcosts',
loadChildren: './../credit-report-profit-and-costs/credit-report-profit-and-costs.module#CreditReportProfitAndCostsPageModule'
},
// {
//   path: 'summary',
//   loadChildren: './../credit-report/credit-report.module#CreditReportPageModule'
// },
// {
//   path: 'summary',
//   loadChildren: './../credit-report/credit-report.module#CreditReportPageModule'
// },
  {
    path: '',
    redirectTo: 'summary'
  }]},
  //   { path: 'annualoverview',
  //     component: CreditReportAnnualOverviewPage
  //   },
  //   { path: 'companyinfo',
  //   component: CreditReportCompanyInfoPage
  // },
  // { path: 'employees',
  // component: CreditReportEmployeesPage
  // },
  // { path: 'financialinfo',
  // component: CreditReportFinancialInfoPage
  // },
  // { path: 'profitandcosts',
  // component: CreditReportProfitAndCostsPage
  // },
  // ]
  //   loadChildren: './../credit-report/credit-report.module#CreditReportPageModule'
  // },
  { path: '',
    component: PortfolioPage
},
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class PortfolioPageRoutingModule {}
