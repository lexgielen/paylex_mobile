import { CreditReportFinancialInfoPageModule } from './../credit-report-financial-info/credit-report-financial-info.module';
import { CreditReportProfitAndCostsPageModule } from './../credit-report-profit-and-costs/credit-report-profit-and-costs.module';
import { CreditReportEmployeesPageModule } from './../credit-report-employees/credit-report-employees.module';
import { CreditReportCompanyInfoPageModule } from './../credit-report-company-info/credit-report-company-info.module';
import { CreditReportAnnualOverviewPageModule } from './../credit-report-annual-overview/credit-report-annual-overview.module';
import { PortfolioCompanyItemComponent } from './../../components/portfolio-company-item/portfolio-company-item';
import { PortfolioPage } from './portfolio';
import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgCircleProgressModule } from 'ng-circle-progress';
import { PortfolioPageRoutingModule } from './portfolio.router.module';
import { CreditReportPageModule } from '../credit-report/credit-report.module';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    NgCircleProgressModule.forRoot({
      // set defaults here
      radius: 100,
      outerStrokeWidth: 16,
      innerStrokeWidth: 8,
      outerStrokeColor: '#78C000',
      innerStrokeColor: '#C7E596',
      animationDuration: 300
    }),
    PortfolioPageRoutingModule,
    CreditReportAnnualOverviewPageModule,
    CreditReportCompanyInfoPageModule,
    CreditReportEmployeesPageModule,
    CreditReportProfitAndCostsPageModule,
    CreditReportFinancialInfoPageModule,
    CreditReportPageModule
  ],
  declarations: [PortfolioPage, PortfolioCompanyItemComponent]
})
export class PortfolioPageModule {}
