import { InAppPurchase } from '@ionic-native/in-app-purchase/ngx';
import { ApiService } from './../../services/api.service';
import { Company } from './../../models/company';
import { NavController, NavParams } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'page-portfolio',
  templateUrl: 'portfolio.html'
})

export class PortfolioPage implements OnInit{
  filtertext: string;
  companies: any[] = [];


constructor(public navCtrl: NavController, private iap: InAppPurchase, private apiService: ApiService) {
  this.iap
  .getProducts(['testproduct'])
  .then((products) => {
    console.log(products);
     //  [{ productId: 'com.yourapp.prod1', 'title': '...', description: '...', price: '...' }, ...]
  })
  .catch((err) => {
    console.log(err);
  });
}

  onInput(x) {
      this.searchCompany(this.filtertext);
  }

  searchCompany(term) {
      // TODO
  }

  buyProduct() {
    this.iap
  .buy('testproduct')
  .then((data) => {
    console.log(data);
    // {
    //   transactionId: ...
    //   receipt: ...
    //   signature: ...
    // }
  })
  .catch((err) => {
    console.log(err);
  });
  }

  ngOnInit() {
    this.apiService.GetCompanies(0, 'ahold', null).subscribe(data => {
      data.entities.forEach(element => {
        this.companies.push(element.entity);
      });
      }, error => {
    alert(`error in call: ${error.message}, status: ${error.status}`);
    });
  }
}
