import { TabsPageRoutingModule } from './tabs.router.module';
import { TabsPage } from './tabs';
import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

@NgModule({
  declarations: [
    TabsPage,
  ],
  imports: [
    IonicModule,
    TabsPageRoutingModule
  ]
})
export class TabsPageModule {}
