import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { CreditReportProfitAndCostsPage } from './credit-report-profit-and-costs';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [
    CreditReportProfitAndCostsPage
  ],
  imports: [
    IonicModule,
    CommonModule,
    RouterModule.forChild([{path: '', component: CreditReportProfitAndCostsPage}])
  ]
})
export class CreditReportProfitAndCostsPageModule {}
