import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CreditReportAnnualOverviewPage } from './credit-report-annual-overview';
import { AnnualfinanceComponent } from 'src/app/components/annualfinance/annualfinance';

@NgModule({
  declarations: [
    AnnualfinanceComponent,
    CreditReportAnnualOverviewPage
  ],
  imports: [
    IonicModule,
    CommonModule,
    RouterModule.forChild([{path: '', component: CreditReportAnnualOverviewPage}])
  ]
})
export class CreditReportAnnualOverviewPageModule {}
