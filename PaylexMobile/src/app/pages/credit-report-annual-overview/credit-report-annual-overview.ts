import { DataShareService } from './../../services/datashare.service';
import { PortfolioPage } from './../portfolio/portfolio';
import { CompanyService } from './../../services/company.service';
import { CreditReportPage } from './../credit-report/credit-report';
import { Component, ViewChild, OnInit } from '@angular/core';
import { NavController, NavParams, MenuController } from '@ionic/angular';
import { Router, ActivatedRoute } from '@angular/router';


@Component({
  selector: 'page-credit-report-annual-overview',
  templateUrl: 'credit-report-annual-overview.html',
})
export class CreditReportAnnualOverviewPage implements OnInit {

  report: any;
  navOptions = {
    animate: true,
    animation: 'ios-transition',
    direction: 'forward'
};
  financialyears = [];

  constructor(public navCtrl: NavController,
    private companyService: CompanyService, private menu: MenuController,
    private router: Router, private dataShareService: DataShareService, private route: ActivatedRoute) {
  }
  ionViewWillEnter(){
    this.menu.close();
  }
  ngOnInit() {
    //this.menu.enable(true, 'creditreport');
    //this.menu.close('creditreport').then(result => {
      if(!this.isNullOrEmptyOrUndefined(this.dataShareService.get('report'))) {
        this.report = this.dataShareService.get('report');
        this.report.entity.balance_sheets.forEach(element => {
          this.financialyears.push({
            year: element.financial_year,
            workcapital: element.working_capital,
            networth: element.net_worth
          });
        });
        for (let index = 0; index < this.report.entity.balance_sheets.length; index++) {
          this.financialyears[index].liquidityratio = this.report.entity.ratios[index].liquidity_ratio;
        }
      } else {
        this.router.navigate(['/tabs/portfolio']);
      }
    //});
  }

  isNullOrEmptyOrUndefined (value) {
    return !value;
}
  CloseReport() {
    this.menu.enable(false);
    this.navOptions.direction = 'backwards';
    // this.navCtrl.setRoot(PortfolioPage, this.report, this.navOptions);
    this.navOptions.direction = 'forward';
  }

  openMenu() {
    this.menu.enable(true, 'creditreport');
    this.menu.open('creditreport');
  }
}
