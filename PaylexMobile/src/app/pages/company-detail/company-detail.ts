import { CompanyDetailPayedPage } from './../company-detail-payed/company-detail-payed';
import { Component, ElementRef, ViewChild } from '@angular/core';
import { NavController, NavParams } from '@ionic/angular';
declare var google: any;
@Component({
  selector: 'page-company-detail',
  templateUrl: 'company-detail.html'
})
export class CompanyDetailPage {
  company: any;
  @ViewChild('map') mapRef: ElementRef;
  map: any;

  constructor(
    public navParams: NavParams, public navctrl: NavController
  ) {}

  ionViewWillEnter() {
    this.company = this.navParams.data.company;
    const lat = parseFloat(this.company.entity.establishments[0].address.location_lat);
    const lng = parseFloat(this.company.entity.establishments[0].address.location_lon);
    const infoWindow = new google.maps.InfoWindow({
      content: `<h5>` + this.company.entity.registered_company_name + `</h5>`
    });
    const image = {
      url: '/assets/imgs/paylex-pin.svg'
    };
    const marker = new google.maps.Marker({
      position: {lat: lat, lng: lng},
      map: this.map,
      title: 'test',
      icon: image
    });

    this.map.setCenter(new google.maps.LatLng(
        parseFloat(this.company.entity.establishments[0].address.location_lat),
        parseFloat(this.company.entity.establishments[0].address.location_lon)));

    marker.addListener('click', () => {
      infoWindow.open(this.map, marker);
    });
  }

  ionViewDidLoad() {
    this.initMap();
  }

  goToPayedDetails(company) {
    //this.navctrl.push(CompanyDetailPayedPage, {company: company});
  }

  initMap() {
    const mapStyles = [
      {
          'featureType': 'administrative',
          'elementType': 'labels.text.fill',
          'stylers': [
              {
                  'color': '#636E72'
              }
          ]
      },
      {
          'featureType': 'administrative.country',
          'elementType': 'labels.text',
          'stylers': [
              {
                  'visibility': 'off'
              }
          ]
      },
      {
          'featureType': 'road',
          'elementType': 'labels',
          'stylers': [
              {
                  'visibility': 'off'
              }
          ]
      },
      {
          'featureType': 'landscape',
          'elementType': 'all',
          'stylers': [
              {
                  'color': '#f5f5f5'
              }
          ]
      },
      {
          'featureType': 'administrative.locality',
          'elementType': 'labels',
          'stylers': [
              {
                  'visibility': 'off'
              }
          ]
      },
      {
          'featureType': 'poi',
          'elementType': 'all',
          'stylers': [
              {
                  'visibility': 'off'
              }
          ]
      },
      {
          'featureType': 'road',
          'elementType': 'all',
          'stylers': [
              {
                  'saturation': -100
              },
              {
                  'lightness': 45
              }
          ]
      },
      {
          'featureType': 'road.highway',
          'elementType': 'all',
          'stylers': [
              {
                  'visibility': 'simplified'
              }
          ]
      },
      {
          'featureType': 'road.arterial',
          'elementType': 'labels',
          'stylers': [
              {
                  'visibility': 'off'
              }
          ]
      },
      {
          'featureType': 'road.arterial',
          'elementType': 'labels.icon',
          'stylers': [
              {
                  'visibility': 'off'
              }
          ]
      },
      {
          'featureType': 'road.highway',
          'stylers': [
              {
                  'visibility': 'simplified'
              }
          ]
      },
      {
          'featureType': 'road.highway',
          'elementType': 'labels',
          'stylers': [
              {
                  'visibility': 'off'
              }
          ]
      },
      {
          'featureType': 'transit',
          'elementType': 'all',
          'stylers': [
              {
                  'visibility': 'off'
              }
          ]
      },
      {
          'featureType': 'water',
          'elementType': 'all',
          'stylers': [
              {
                  'color': '#91D5DD'
              },
              {
                  'visibility': 'on'
              }
          ]
      }
  ];
    const options = {zoom: 15, styles: mapStyles};
    this.map = new google.maps.Map(this.mapRef.nativeElement, options);

  }
}
