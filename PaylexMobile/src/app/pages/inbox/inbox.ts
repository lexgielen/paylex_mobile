import { CompanyService } from './../../services/company.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'page-inbox',
  templateUrl: 'inbox.html',
  providers: [CompanyService],
  styleUrls: ['inbox.scss']
})
export class InboxPage implements OnInit {

  filtertext = '';

  onSearch(val: string) {
    this.filtertext = val;
  }

  ngOnInit(): void {
    // this.getCompanies();
  }
}

