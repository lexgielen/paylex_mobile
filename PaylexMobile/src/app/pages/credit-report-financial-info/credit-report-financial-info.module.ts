import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CreditReportFinancialInfoPage } from './credit-report-financial-info';

@NgModule({
  declarations: [
    CreditReportFinancialInfoPage,

  ],
  imports: [
    IonicModule,
    CommonModule,
    RouterModule.forChild([{path: '', component: CreditReportFinancialInfoPage}])
  ]
})
export class CreditReportFinancialInfoPageModule {}
