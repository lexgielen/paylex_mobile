import { PortfolioPage } from './../portfolio/portfolio';
import { Component, ViewChild, OnInit } from '@angular/core';
import {  MenuController, NavController, NavParams } from '@ionic/angular';
import { CompanyService } from '../../services/company.service';
import { Router, ActivatedRoute } from '@angular/router';
import { DataShareService } from 'src/app/services/datashare.service';

@Component({
  selector: 'page-credit-report-financial-info',
  templateUrl: 'credit-report-financial-info.html',
})
export class CreditReportFinancialInfoPage implements OnInit{

  report: any;
  navOptions = {
    animate: true,
    animation: 'ios-transition',
    direction: 'forward'
};
constructor(public navCtrl: NavController, private menu: MenuController,
  private router: Router, private dataShareService: DataShareService, private route: ActivatedRoute) {
}

ionViewWillEnter(){
  this.menu.close();
}
ngOnInit() {
  if(!this.isNullOrEmptyOrUndefined(this.dataShareService.get('report'))) {
    this.report = this.dataShareService.get('report');
  } else {
    this.router.navigate(['/tabs/portfolio']);
  }
}
openMenu() {
  this.menu.enable(true, 'creditreport');
  this.menu.open('creditreport');
}
isNullOrEmptyOrUndefined (value) {
  return !value;
}
  setActiveReputation(value) {
    if (value < 40) {
      document.getElementById('reputation-bad').classList.add('active');
      document.getElementById('reputation-ok').classList.remove('active');
      document.getElementById('reputation-good').classList.remove('active');
    } else if (value < 70) {
      document.getElementById('reputation-bad').classList.remove('active');
      document.getElementById('reputation-ok').classList.add('active');
      document.getElementById('reputation-good').classList.remove('active');
    } else {
      document.getElementById('reputation-bad').classList.remove('active');
      document.getElementById('reputation-ok').classList.remove('active');
      document.getElementById('reputation-good').classList.add('active');
    }
  }
  ionViewDidEnter() {
    this.setActiveReputation(this.report.entity.credit_scores[0].current_common_credit_rating);
  }

  CloseReport() {
    this.menu.enable(false);
    this.navOptions.direction = 'backwards';
    this.navOptions.direction = 'forward';
  }
}
