import { DashboardPage } from './dashboard';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: 'search',
  loadChildren: './../search/search.module#SearchPageModule'
},
{
  path: '', component: DashboardPage
},
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class DashboardPageRoutingModule {}
