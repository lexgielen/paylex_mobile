import { SearchPage } from './../search/search';
import { NavController } from '@ionic/angular';
import { Component } from '@angular/core';

@Component({
  selector: 'page-dashboard',
  templateUrl: 'dashboard.html',
})
export class DashboardPage {
  filtertext = '';
  constructor(public navCtrl: NavController) {

  }
  onSearch(val: string) {
    // this.navCtrl.push(SearchPage)
    // this.filtertext=val;
  }

  searchbarFocus() {
    // this.navCtrl.push(SearchPage);
  }

  ionViewDidEnter() {
    const elements = document.querySelectorAll('.tabbar');

    if (elements != null) {
      Object.keys(elements).map((key) => {
        elements[key].style.display = 'flex';
      });
    }
  }
}

