import { ApiService } from './services/api.service';
import { CallsService } from './services/calls.service';
import { DataShareService } from './services/datashare.service';
import { TabsPageModule } from './pages/tabs/tabs.module';
import { TabsPage } from './pages/tabs/tabs';
import { PortfolioPage } from './pages/portfolio/portfolio';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy, Routes, RouterModule } from '@angular/router';

import { IonicModule, IonicRouteStrategy, MenuController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { DashboardPage } from './pages/dashboard/dashboard';
import { InAppPurchase } from '@ionic-native/in-app-purchase/ngx';
import { HttpClient, HttpClientModule } from '@angular/common/http';



@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [BrowserModule,
    IonicModule.forRoot(),
    TabsPageModule,
    HttpClientModule,
    AppRoutingModule],
  providers: [
    CallsService,
    ApiService,
    DataShareService,
    InAppPurchase,
    StatusBar,
    SplashScreen,
    MenuController,
    HttpClient,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
